﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoordinateGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                //Console.WriteLine("Range X from:");
                //var minimumX = Convert.ToDouble(Console.ReadLine());
                //Console.WriteLine("Range X to:");
                //var maximumX = Convert.ToDouble(Console.ReadLine());
                //Console.WriteLine("Range Y from:");
                //var minimumY = Convert.ToDouble(Console.ReadLine());
                //Console.WriteLine("Range Y to:");
                //var maximumY = Convert.ToDouble(Console.ReadLine());
                var clicker = new CoordinateClicker(0, 0, 0, 0);
                clicker.ShowDialog();
                //double maxX =0;
                //double maxY = 0;
                //double minX = 0;
                //double minY = 0;
                //foreach (var t in clicker.Points)
                //{
                //    if (t.X > maxX)
                //    {
                //        maxX = t.X;
                //    }
                //    if (t.Y > maxY)
                //    {
                //        maxY = t.Y;
                //    }
                //    if (t.X > minX)
                //    {
                //        minX= t.X;
                //    }
                //    if (t.Y > minY)
                //    {
                //        minY = t.Y;
                //    }
                //}
                var writer = new StreamWriter("data.txt");
                foreach (var point in clicker.Points)
                {
                    //var x = ((maximumX - minimumX)*(point.X - minX)/(maxX - minX)) + minimumX;
                    //var y = ((maximumY - minimumY) * (point.Y - minY) / (maxY - minY)) + minimumY;
                    Console.WriteLine(point.X + ", " + point.Y*-1);
                    writer.Write(point.X + ", " + point.Y * -1);
                }
                writer.Flush();
                Console.WriteLine("Press a key to continue");
                Console.ReadLine();
            }
        }
    }
}
