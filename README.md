# Coordinate clicker #

### What is this repository for? ###

Coordinate clicker is a simple tool to quickly create list of points simply by clicking on a displayed X,Y axis.
It takes all created points and writes them to a console window and saves to them to a .txt file.
Its main purpose is to create data for [linear/polynomial regression ](https://bitbucket.org/projekt53/machine-learning) testing.

### Screenshots ###

![coordinate.png](https://bitbucket.org/repo/Mjnp9o/images/2841660994-coordinate.png)